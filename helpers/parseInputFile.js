
const parseInputFile = async (file) => {
  const res = []
  const m0 = new Date().getTime()
  var lines = require('fs').readFileSync(file, 'utf-8')
    .split('\n')
    .filter(Boolean)
    lines.forEach(line => {
      if(!line.startsWith('  ')) {
        res.push(readPerson(line))
      }
      else {
        res[res.length-1].prose.push(line.substr(2))
      }
  })
  return res
}

const readPerson = (line) => {
  const surname = line.split(', ')[0]
  const firstname = line.split(', ')[1].split(' -- ')[0]
  return { surname, firstname, prose:[] }
}

module.exports.parseInputFile = parseInputFile