const { parseInputFile } = require('./helpers/parseInputFile')

const inputFile = './test-data-10-exp-5.list'

const app = async () => {
  const people = await parseInputFile(inputFile)
  
  // question 1
  console.log('Count of unique first names: ', countUnique(people, person => person.firstname))
  console.log('Count of unique surnames: ', countUnique(people, person => person.surname))
  console.log('Count of unique full names: ', countUnique(people, person => person.surname+', '+person.firstname))

  // question 2 - ten most common first names
  console.log('Most common first names')
  mostCommon(people, 'firstname', 10)
  .forEach(itm => {
    console.log(Object.keys(itm)[0], ' (',itm[Object.keys(itm)[0]] ,')')
  })

  // question 3
  console.log('Most common last names')
  mostCommon(people, 'surname', 10)
  .forEach(itm => {
    console.log(Object.keys(itm)[0], ' (',itm[Object.keys(itm)[0]] ,')')
  })
}

const countUnique = (collection, funct) => 
  new Set(collection.map(funct)).size

const mostCommon = (collection, property, count) =>{
  const unique = {}
  collection.forEach(itm => {
    unique[itm[property]] = (unique[itm[property]] || 0) +1
  })
  return Object.keys(unique).map((key) => {
    return {[key]: unique[key]}
  }).sort((a,b)=> {
    return b[Object.keys(b)[0]] - a[Object.keys(a)[0]]
  }).slice(0, count)
}

app()